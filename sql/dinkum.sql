DROP TABLE "places";
CREATE TABLE IF NOT EXISTS "places" (
  "id" SERIAL PRIMARY KEY,
  "place_name" varchar(80) NOT NULL,
  "place_type" varchar(80) NOT NULL,
  "subtype" varchar(80),
  "place_label" varchar(255),
  "shapetype" varchar(20) DEFAULT 'point',
  "point" point,
  "shape" polygon,
  "street_address" varchar(200),
  "suburb" varchar(50),
  "state" varchar(20),
  "postcode" integer
); 