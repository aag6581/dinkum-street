
// shapes containing a point
select * from places where point '(-34.882264204,138.661498973)' <@ shape;

// closest "thing" to point
select *, '(-34.882264204,138.661498979)' <-> point AS distance
from places 
order by '(-34.882264204,138.661498973)' <-> point ASC
limit 1;

