package main

import (
    "fmt"
    //"dinkumstreet/gda94"
    "os"
    //"strconv"
    "log"
    "github.com/jonas-p/go-shp"
    "reflect"
    "strings"
    "encoding/json"
    "io/ioutil"
    "dinkumstreet/gda94"
    "dinkumstreet/utils"
)

type Mapper struct {
    NAME string
    TYPE string
    SUBTYPE string
    LABEL string
    POSTCODE string
    ADDRESS string
    SUBURB string
    STATE string
    POINT string
    SHAPE string
    GDA94 bool
}

/* load json mapping file */
func loadMapping( shapefile string ) (Mapper, error) {
    var m Mapper
    mapfile := shapefile+".json"
    
    b, err := ioutil.ReadFile( mapfile )
    if err != nil {
        return m, err
    }
    
    je := json.Unmarshal( b, &m )
    return m, je
}

/* Build a PG SQL Poly type from points */
func buildPolygon( xlate bool, points []shp.Point ) string {
    
    var parts []string
    
    for _, p := range points {
        
        // convert point to string -- use fmt
        var s string
        
        if xlate {
            
            g := &gda94.GDA94Point{ X: p.X, Y: p.Y }
            
            gps, _ := g.ToLatLong()
            
            s = fmt.Sprintf( "(%.9f,%.9f)", gps.Latitude, gps.Longitude )
        } else {
            s = fmt.Sprintf( "(%.9f,%.9f)", p.Y, p.X )
        }
        parts =  append( parts, s )
        
    }
    
    return "(" + strings.Join( parts, "," ) + ")"
}

func buildPoint( xlate bool, p *shp.Point ) string {
    
    var s string
    
    if xlate {
        
        g := &gda94.GDA94Point{ X: p.X, Y: p.Y }
        
        gps, _ := g.ToLatLong()
        
        s = fmt.Sprintf( "(%.9f,%.9f)", gps.Latitude, gps.Longitude )
    } else {
        s = fmt.Sprintf( "(%.9f,%.9f)", p.Y, p.X )
    }
    
    return s
}

func getMapping( metadata map[string]string, name string ) string {
    
    if strings.HasPrefix( name, "@" ) {
        return strings.Replace( name, "@", "", 1 )
    } else {
        return metadata[name]
    }
    
}

func main() {
    var dink *utils.GeoDB = &utils.GeoDB{}

    db, conerr := dink.Connect();
    if conerr != nil {
        log.Fatalf("Error connecting - unfair dinkum: %v\n", conerr.Error() )
    }
    //defer db.Close()
    
    if len(os.Args) < 2 {
        fmt.Printf("usage: %v <shapefile_base>\n", os.Args[0])
        os.Exit(1)
    }
    
    shapefile := os.Args[1]
    fmt.Printf("Using GRS shapefile with name [%s]\n", shapefile)
    
    shape, err := shp.Open(shapefile)
    if err != nil { 
        log.Fatal(err) 
    } 
    defer shape.Close()
    fields := shape.Fields()
    
    fmt.Printf( "Metadata contained within file %v\n", fields )
    
    // load mapping
    mapping, merr := loadMapping( shapefile )
    if merr != nil {
        log.Fatalf("Load aborted due to error: %v\n", merr.Error())
    }
    
    fmt.Printf("Using mapping [%v]\n", mapping)
    
    //os.Exit(0)
    
    // iterate through shapefile
    for shape.Next() {
        n, p := shape.Shape()
            // print feature
        
        shapeType := reflect.TypeOf(p).Elem()
        
        fmt.Println(shapeType)
        
        point, ok := p.(*shp.Point)
        
        var polydata string  = "(0,0)"
        var pointdata string = "(0,0)"
        var shapetype string

	fmt.Println(shapetype)
        
        if ok {
            pointdata = buildPoint( mapping.GDA94, point )
            fmt.Println("POINT", pointdata)
            shapetype = "POINT"
        } else {
            poly, ok := p.(*shp.Polygon)
            if ok {
                polydata =  buildPolygon(mapping.GDA94, poly.Points)
                fmt.Println("POLYGON", polydata)
                shapetype = "SHAPE"
            } else {
                fmt.Println("NO IDEA")
            }
        }
        
        var metadata map[string]string
        metadata = make(map[string]string)
        
        // print attributes
        for k, f := range fields {
            val := shape.ReadAttribute(n, k)
            fmt.Printf("%v)\t[%v]: %v\n", k, f, val)
            // fix for DBF badness
            label := strings.Trim(fmt.Sprintf("%v",f), "\000")
            metadata[label] = val
        }
        
        // var mapper
        var data Mapper
        
        data.NAME       = getMapping(metadata, mapping.NAME)
        data.LABEL      = getMapping(metadata, mapping.LABEL)
        data.ADDRESS    = getMapping(metadata, mapping.ADDRESS)
        data.SUBURB     = getMapping(metadata, mapping.SUBURB)
        data.TYPE       = getMapping(metadata, mapping.TYPE)
        data.SUBTYPE    = getMapping(metadata, mapping.SUBTYPE)
        data.POINT      = pointdata
        data.SHAPE      = polydata
        data.POSTCODE   = getMapping(metadata, mapping.POSTCODE)
        data.STATE      = getMapping(metadata, mapping.STATE)
        
        //fmt.Printf( "INFO: [%v]\n", metadata["SUBURB"])

      
        fmt.Println()
    }
    
    db.Close()
}
