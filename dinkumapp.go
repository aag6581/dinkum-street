package main

/* 
 * This file contains the Yewsi API service written in the Gin Framework... 
 * Going past 88mph and beyond!!! 
 */

import (
    "github.com/gin-gonic/gin"
    "github.com/kellydunn/golang-geo"
    "dinkumstreet/utils"
    //"fmt"
    "log"
    "database/sql"
    "net/http"
    "strings"
    "math"
)

const (
    PORT = ":80"
)

var dink *utils.GeoDB
var db *sql.DB

func isItDinkum( c *gin.Context ) {
    
        c.Req.ParseForm()

        address_1 := c.Req.PostForm["address_1"][0]
        address_2 := c.Req.PostForm["address_2"][0]
        
        g1 := geo.GoogleGeocoder{}
        p1, gerr1 := g1.Geocode(address_1)
        if gerr1 != nil {
            c.String(500, "Something bad happened looking up address")
            return
        }
        
        g2 := geo.GoogleGeocoder{}
        p2, gerr2 := g2.Geocode(address_2)
        if gerr2 != nil {
            c.String(500, "Something bad happened looking up address")
            return
        }
        
        // get it
        r1, qerr1 := dink.DistancesForLocation( p1.Lat(), p1.Lng() )
        
        if qerr1 != nil {
            c.String(500, "Something bad happened calculating the score")
            return
        }
        
        r2, qerr2 := dink.DistancesForLocation( p2.Lat(), p2.Lng() )
        
        if qerr2 != nil {
            c.String(500, "Something bad happened calculating the score")
            return
        }
        
        // add up score
        var score1 float64 = 0;
        var cats1 []string
        for k1, v1 := range r1 {
            score1 = score1 + v1;
            cats1 = append( cats1, k1 )
        }
        score1 = math.Floor(100*score1)
        catstr1 := strings.Join( cats1, ", " )
        
        // add up score
        var score2 float64 = 0;
        var cats2 []string
        for k2, v2 := range r2 {
            score2 = score2 + v2;
            cats2 = append( cats2, k2 )
        }
        score2 = math.Floor(100*score2)
        catstr2 := strings.Join( cats2, ", " )
      
        obj := gin.H{   
            "title": "Results", 
            "address1": strings.ToUpper(address_1), 
            "lat1": p1.Lat(), 
            "lng1": p1.Lng(), 
            "categories1": catstr1, 
            "score1": score1, 
            "categories2": catstr2, 
            "score2": score2, 
            "address2": strings.ToUpper(address_2), 
            "lat2": p2.Lat(), 
            "lng2": p2.Lng(), 
        }
        
        c.HTML(200, "dinkresults.tmpl", obj)
}

func defaultPage( c *gin.Context ) {
        obj := gin.H{"title": "Welcome to Dinkum Street"}
        c.HTML(200, "dinkquery.tmpl", obj)
}

/* Setup our API endpoints */
func establishEndpoints( r *gin.Engine ) {
    
    dink = &utils.GeoDB{}

    var conerr error
    
    db, conerr = dink.Connect();
    if conerr != nil {
        log.Fatalf("Error connecting - unfair dinkum: %v\n", conerr.Error() )
    }

    r.LoadHTMLTemplates("templates/*")
    
    r.GET( "/", defaultPage )
    r.POST( "/isitdinkum", isItDinkum )
    
    r.ServeFiles("/images/*filepath", http.Dir("public/images"))
    
}

func main() {

    r := gin.Default()
    
    establishEndpoints(r)
    
    r.Run(PORT)
    
}
