/* 
This package converts GDA94 (LCC projection) co-ordinates to Latitude and Longitude
values using the PROJ.4 library.

Author: April Ayres-Griffiths

Project: Dinkum Street (GovHack Adelaide Unleashed, 2014)

Note:
Transform parameters are taken from: http://www.atlas.sa.gov.au/help/frequently-asked-questions
*/
package gda94

import (
    "fmt"
    "github.com/pebbe/go-proj-4/proj"
    "log"
    "math"
)

/* Magic Lives here */
const (
    xSRC_PROJ_INIT = "+proj=lcc +ellps=GRS80 +towgs84=0,0,0 +x_0=1000000 +y_0=2000000 +pm=135 +lat_1=-28 +lat_2=-36 +lat_0=-32"
    xDST_PROJ_INIT = "+proj=latlong +ellps=GRS80 +towgs84=0,0,0"
)

/* Point location in the GDA94 standard */
type GDA94Point struct {
    X float64
    Y float64
}

/* Point in standard GPS lat / long pairing */
type GPSCoordinate struct {
    Latitude float64
    Longitude float64
}

/* We keep state here for conversions so we don't have to reinit each time */
var (
    xGDA94 *proj.Proj
    xLATLON *proj.Proj
    errGDA94 error
    errLATLON error
)

/* Setup transforms */
func initTransforms() error {
    
    if xGDA94 != nil && xLATLON != nil {
        return nil
    }
    
    log.Printf("Init transform: %s", xSRC_PROJ_INIT)
    xGDA94, errGDA94 = proj.NewProj(xSRC_PROJ_INIT)
    //defer GDA94.Close()
    if errGDA94 != nil {
        fmt.Printf("Problem initializing GDA94 projection [%v]\n", errGDA94.Error())
        return errGDA94
    }
    
    log.Printf("Init transform: %s", xDST_PROJ_INIT)
    xLATLON, errLATLON = proj.NewProj(xDST_PROJ_INIT)
    //defer LATLON.Close()
    if errLATLON != nil {
        fmt.Printf("Problem initializing LAT/LONG projection [%v]\n", errLATLON.Error())
        return errLATLON         
    }
    
    return nil
}

func (point *GDA94Point)ToLatLong() (GPSCoordinate, error) {
    
    ierr := initTransforms()
    
    if ierr != nil {
        log.Fatalf( "Failed to init transforms: %v", ierr.Error() )
    }
    
    lon, lat, err := proj.Transform2( xGDA94, xLATLON, point.X, point.Y )
    if err != nil {
            return GPSCoordinate{}, err
    }
    
    gps := GPSCoordinate{ Latitude: proj.RadToDeg(lat), Longitude: proj.RadToDeg(lon)}
    
    return gps, nil
}

/* Converts decimal degrees (float64) to a DMS string suitable for Google maps */
func Dec2DMS( dec float64 ) string {

  D := int(math.Floor( math.Abs( dec ) ))  
  
  M := int((math.Abs(dec) - math.Floor(math.Abs(dec))) * 60)
  
  S := ((math.Abs(dec) - math.Floor(math.Abs(dec))) - (float64(M)/float64(60))) * 3600
  
  return fmt.Sprintf( "%d°%d'%05.2f\"", D, M, S )
    
}

func (point *GDA94Point)ToLatLonString() (string, error) {
    
    gps, err := point.ToLatLong()
    if err != nil {
        return "", err
    }
    
    // let us format it
    lat_S := "S"
    lon_S := "E"
    if gps.Latitude > 0 {
        lat_S = "N"
    }
    if gps.Longitude < 0 {
        lon_S = "W"
    }
    
    return Dec2DMS(gps.Latitude)+lat_S+"+"+Dec2DMS(gps.Longitude)+lon_S, nil
    
}
