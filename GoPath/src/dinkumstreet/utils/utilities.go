package utils

import (
    "fmt"
    //"dinkumstreet/gda94"
    //"os"
    "strconv"
    "log"
    "database/sql"
    _ "github.com/lib/pq"
    "strings"
)

const (

    xURL = "postgres://dinkum:01!vebat@localhost/dinkum?sslmode=disable"
    
)

type GeoDB struct {

    DB *sql.DB
    
}

func (d *GeoDB) Connect() (*sql.DB, error) {
    db, err := sql.Open("postgres", xURL)
    if err != nil {
        log.Fatal(err)
    }  
    d.DB = db
    err = d.DB.Ping()
    return d.DB, err
}

func (dinkum *GeoDB) Insert( name string, label string, placetype string, subtype string, shapetype string, pointdata string, polydata string, suburb string, postcode string, address string, state string ) error {
    
    stmt, err := dinkum.DB.Prepare(`INSERT INTO places(place_name, place_type, subtype, place_label, point, shape, shapetype, suburb, postcode, street_address, state)
    VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)`)
    if err != nil {
    
        log.Fatalf("Insert prepare failed: %v %v\n", err.Error(), stmt)
        
    }
    
    res, err := stmt.Exec( name, placetype, subtype, label,  pointdata, polydata, shapetype, suburb, postcode, address, state )
    fmt.Printf("Insert result = %v %v\n", res, err)
    
    return err
}

func (dinkum *GeoDB) FindNearest( place_type string, lat float64, lng float64 ) (*sql.Rows, error) {
    
    s := fmt.Sprintf( "(%.9f,%.9f)", lat, lng )
    
    res, err := dinkum.DB.Query("select *, $1 <-> point AS distance from places where shapetype = 'POINT' and place_type = $2 order by $3 <-> point ASC limit 1", s, place_type, s)
    if err != nil {
    
        log.Fatalf("Query nearest prepare failed: %v %v\n", err.Error(), res)
        
    }
   
    return res, err
}

func (dinkum *GeoDB) DistanceToNearestPOINT( place_type string, subtype string, lat float64, lng float64 ) (bool, float64, error) {
    
    s := fmt.Sprintf( "(%.9f,%.9f)", lat, lng )
    
    res, err := dinkum.DB.Query("select *, $1 <-> point AS distance from places where shapetype = 'POINT' and place_type = $2 and subtype like $3 order by $4 <-> point ASC limit 1", s, place_type, subtype, s)
    if err != nil {
    
        log.Fatalf("Query nearest prepare failed: %v %v\n", err.Error(), res)
        
    }
    
    var id,place_name,xplace_type,xsubtype,place_label,shapetype,point,shape,street_address,suburb,state,postcode,distance string
    var ee error
    for res.Next() {
                ee = res.Scan(&id,&place_name,&xplace_type,&xsubtype,&place_label,&shapetype,&point,&shape,&street_address,&suburb,&state,&postcode,&distance)
    }
    
    if ee != nil {
    
        return false, 999999999, ee
        
    }    
    
    d, de := strconv.ParseFloat( distance, 64 )
    if de != nil {
        return false, 999999999, de
    }
    
    // convert distance to KM
    km := 111.3199 * d
   
    return true, km, nil 
}

func (dinkum *GeoDB) DistancesForLocation(lat, lon float64) (map[string]float64, error) {

    things := [9]string{
            "CENTRELINK:",
            "TRANSIT:",
            "EMERGENCY SERVICE:Ambulance",
            "EMERGENCY SERVICE:Fire",
            "EMERGENCY SERVICE:Police",
            "HOSPITAL:",
            "SCHOOL:Primary Schools",
            "SCHOOL:High Schools",
            "SCHOOL:Preschool",
    }
    
    var metadata map[string]float64
    metadata = make(map[string]float64)
    
    for _, v := range things {

        parts := strings.SplitN( v, ":", 2 )

        ok, distance, err := dinkum.DistanceToNearestPOINT( parts[0], "%"+parts[1]+"%", lat, lon )
        if ok {
            metadata[v] = distance
        } else {
            return metadata, err
        }
    }
    
    return metadata, nil
}