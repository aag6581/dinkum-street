#!/usr/bin/env perl

use XML::Simple;
use Data::Dumper;
 
my $ref = XMLin($ARGV[0], ForceArray => []);

#print Data::Dumper->Dump([$ref])."\n";

$listref = $ref->{"gml:featureMember"};

print "Y,X,TYPE,SUBTYPE,SUBURB,STATE\n";

foreach my $record (@{$listref}) {
    my $r = $record->{"commswfs:MBBPts"};
    my $locstr = $r->{"commswfs:Obj"}->{"gml:Point"}->{"gml:coordinates"};
    my ($LON, $LAT) = split(/,/, $locstr, 2);
    
    my $MOBILE_QUALITY = $r->{"commswfs:QUALITY_MOBILE_RATING"};
    my $MOBILE_AVAILABILITY = $r->{"commswfs:AVAILABILITY_MOBILE_RATING"};
    my $BROADBAND_QUALITY = $r->{"commswfs:QUALITY"};
    my $SUBURB = $r->{"commswfs:ESA_NAME"};
    my $STATE = $r->{"commswfs:ESA_STATE"};
    
    print "$LAT,$LON,BROADBAND QUALITY,$BROADBAND_QUALITY,$SUBURB,$STATE\n";
    print "$LAT,$LON,MOBILE COVERAGE,$MOBILE_AVAILABILITY,$SUBURB,$STATE\n";
    print "$LAT,$LON,MOBILE DATA,$MOBILE_QUALITY,$SUBURB,$STATE\n";
}
