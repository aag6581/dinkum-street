package main

/* ===========================================================================================
 * This program converts GDA94 (LCC projection) co-ordinates to Latitude and Longitude
 * values using the PROJ.4 library.
 * 
 * Author: April Ayres-Griffiths
 * ===========================================================================================
 */

import (
    "fmt"
    "dinkumstreet/gda94"
    "os"
    "strconv"
)

func main() {

    if len(os.Args) < 3 {
        fmt.Printf( "usage: %v X Y\n", os.Args[0] )
        fmt.Printf( "Convert GDA94 (X, Y) co-ordinates to GPS equivalent.\n" )
        os.Exit(1)
    }
    
    X, errX := strconv.ParseFloat( os.Args[1], 64 )
    if errX != nil {
        fmt.Printf("Error processing X value [%v]\n", os.Args[1])
        os.Exit(2)
    }
  
    Y, errY := strconv.ParseFloat( os.Args[2], 64 )
    if errY != nil {
        fmt.Printf("Error processing Y value [%v]\n", os.Args[2])
        os.Exit(2)
    }
    

    // define a point
    g := &gda94.GDA94Point{ X: X, Y: Y }
    
    s, errTrans := g.ToLatLonString()
    if errTrans != nil {
        fmt.Printf("Problem with transformation: %v\n", errTrans.Error())
        os.Exit(3)
    }
    
    fmt.Printf("https://www.google.com.au/maps/place/%s\n", s)
}