package main

import (
    "encoding/csv"
    "fmt"
    "io"
    "os"
    "encoding/json"
    "io/ioutil"
    "dinkumstreet/gda94"
    "dinkumstreet/utils"
    "github.com/kellydunn/golang-geo"
    "log"
    "strings"
    "strconv"
)

type Mapper struct {
    NAME string
    TYPE string
    SUBTYPE string
    LABEL string
    POSTCODE string
    ADDRESS string
    SUBURB string
    STATE string
    POINT string
    SHAPE string
    GDA94 bool
    X string
    Y string
    GEOCODE string
}

var header_record []string

/* load json mapping file */
func loadMapping( csvfile string ) (Mapper, error) {
    var m Mapper
    mapfile := csvfile+".json"
    
    b, err := ioutil.ReadFile( mapfile )
    if err != nil {
        return m, err
    }
    
    je := json.Unmarshal( b, &m )
    return m, je
}

func buildPoint( xlate bool, lat float64, lon float64 ) string {
    
    var s string
    
    if xlate {
        
        g := &gda94.GDA94Point{ X: lon, Y: lat }
        
        gps, _ := g.ToLatLong()
        
        s = fmt.Sprintf( "(%.9f,%.9f)", gps.Latitude, gps.Longitude )
    } else {
        s = fmt.Sprintf( "(%.9f,%.9f)", lat, lon )
    }
    
    return s
}


func getMapping( metadata map[string]string, name string ) string {
    
    if strings.HasPrefix( name, "@" ) {
        return strings.Replace( name, "@", "", 1 )
    } else {
        return metadata[name]
    }
    
}


func main() {
    
    var dink *utils.GeoDB = &utils.GeoDB{}

    db, conerr := dink.Connect();
    if conerr != nil {
        log.Fatalf("Error connecting - unfair dinkum: %v\n", conerr.Error() )
    }
    defer db.Close()
    
    if len(os.Args) < 2 {
        fmt.Printf("usage: %v <csvfile>\n", os.Args[0])
        os.Exit(1)          
    }
    
    csvfile := os.Args[1]
    fmt.Printf("Using csvfile with name [%s]\n", csvfile)
    
    file, err := os.Open(csvfile)
    if err != nil {
        // err is printable
        // elements passed are separated by space automatically
        fmt.Println("Error:", err)
        return
    }
    // automatically call Close() at the end of current method
    defer file.Close()
    
    // load mapping
    mapping, merr := loadMapping( csvfile )
    if merr != nil {
        log.Fatalf("Load aborted due to error: %v\n", merr.Error())
    }
    // 
    reader := csv.NewReader(file)
    // options are available at:
    // http://golang.org/src/pkg/encoding/csv/reader.go?s=3213:3671#L94
    reader.Comma = ','
    lineCount := 0
    for {
        // read just one record, but we could ReadAll() as well
        record, err := reader.Read()
        // end-of-file is fitted into err
        if err == io.EOF {
            break
        } else if err != nil {
            fmt.Println("Error:", err)
            return
        }
        
        if lineCount == 0 {
            header_record = record
        } else {
            // record is an array of string so is directly printable
            fmt.Println("Record", lineCount, "is", record, "and has", len(record), "fields")
            // and we can iterate on top of that
            
            var metadata map[string]string
            metadata = make(map[string]string)
            
            for i := 0; i < len(record); i++ {
                k := header_record[i]
                v := record[i]
                metadata[k] = v
                //fmt.Println(" ", record[i])
            }
            fmt.Printf("[%v]\n", metadata)
            
            // got metadata
            var polydata string  = "(0,0)"
            var pointdata string = "(0,0)"
            var shapetype string = "POINT"
            
            // var mapper
            var data Mapper
            var address_ok = true
            
            if mapping.GEOCODE != "" {
                
                    address := getMapping(metadata, mapping.GEOCODE)
                    fmt.Println("Lookup: ", address)
    
                    g := geo.GoogleGeocoder{}
                    p, gerr := g.Geocode(address)
                    if gerr != nil {
                        address_ok = false
                    } else {
                        pointdata = buildPoint(mapping.GDA94, p.Lat(), p.Lng())
                    }
                
            } else {
                raw_x := getMapping(metadata, mapping.X)
                raw_y := getMapping(metadata, mapping.Y)
                
                lon, lerr := strconv.ParseFloat( raw_x, 64 )
                lat, larr := strconv.ParseFloat( raw_y, 64 )
                if lerr != nil || larr != nil {
                    log.Fatalf("Issues with point determination: %v %v\n", lerr, larr)
                }
                
                pointdata = buildPoint(mapping.GDA94, lat, lon)
            }
            
            data.NAME       = getMapping(metadata, mapping.NAME)
            data.LABEL      = getMapping(metadata, mapping.LABEL)
            data.ADDRESS    = getMapping(metadata, mapping.ADDRESS)
            data.SUBURB     = getMapping(metadata, mapping.SUBURB)
            data.TYPE       = getMapping(metadata, mapping.TYPE)
            data.SUBTYPE    = getMapping(metadata, mapping.SUBTYPE)
            data.POINT      = pointdata
            data.SHAPE      = polydata
            data.POSTCODE   = getMapping(metadata, mapping.POSTCODE)
            data.STATE      = getMapping(metadata, mapping.STATE)
            
            //fmt.Printf( "INFO: [%v]\n", metadata["SUBURB"])
            if address_ok {
                ei := dink.Insert( data.NAME, data.LABEL, data.TYPE, data.SUBTYPE, shapetype, pointdata, polydata, data.SUBURB, data.POSTCODE, data.ADDRESS, data.STATE )
                if ei != nil {
                
                    log.Fatalf("Failed to insert data: %v\n", ei.Error())
                    
                }
            }
        }
        lineCount += 1
    }
}
